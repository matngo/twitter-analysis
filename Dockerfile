FROM python:3.7

COPY . /app

WORKDIR /app

RUN pip install ".[web, models]"

RUN python -m spacy download en_core_web_sm

CMD gunicorn -b 0.0.0.0:$PORT -w4 "twitter_analysis.web:create_app()"
