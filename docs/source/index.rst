.. Twitter Analysis documentation master file, created by
   sphinx-quickstart on Sun Sep 29 16:14:03 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Twitter Analysis's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

API Reference
-------------
.. toctree::
   :maxdepth: 2
   :caption: API Reference

   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
