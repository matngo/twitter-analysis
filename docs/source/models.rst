NLP Models
----------
.. module:: twitter_analysis.model

Dataset Reader
**************
.. automodule:: twitter_analysis.model.dataset_reader
   :members:

Predictor
*********

.. automodule:: twitter_analysis.model.predictor
   :members:
