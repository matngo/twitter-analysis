# Twitter Analysis

## Introduction
This repo is dedicated to twitter data processing.

## Getting Started

### Demo

Check out the [live demo](http://twitter-analysis.matngo.fr).

You can also only use the prediction model, using the route http://twitter-analysis.matngo.fr/process-sentiment, for instance using the `requests` library.

#### Example

```python
>>> import requests

>>> url = "http://twitter-analysis.matngo.fr/process-sentiment"

# A sad song
>>> negative_text = """
And I find it kinda funny, I find it kinda sad
The dreams in which I'm dying are the best I've ever had
I find it hard to tell you, I find it hard to take
When people run in circles it's a very very
Mad world, mad world"""
>>> r = requests.post(url, data={"tweet": negative_text})
>>> r.json()
{'all_labels': ['positive', 'negative'], 'class_probabilities': [0.19376489520072937, 0.806235134601593], 'label': 'negative', 'logits': [-0.5269206762313843, 0.8988091945648193]}

# A happy song
>>> positive_text = """Everything is awesome, everything is cool when you're part of a team
Everything is awesome, when you're living out a dream"""
>>> r = requests.post(url, data={"tweet": positive_text})
>>> r.json()
{'all_labels': ['positive', 'negative'], 'class_probabilities': [1.0, 1.0550157414357386e-09], 'label': 'positive', 'logits': [10.82830810546875, -9.841402053833008]}
```

### Using docker

#### Installation

```sh
export CONSUMER_KEY=<twitter_api_key>
export CONSUMER_SECRET_KEY=<twitter_secret_api_key>
docker-compose up
```

Don't forget to setup the callback url from your twitter dev account to authorize the url `http://localhost:5000/twitter/login`

### Install as a Python package

```sh
git clone https://gitlab.com/matngo/twitter-analysis.git
cd twitter-analysis
pip install ".[dev, models]"
wget -P models_archive -O model.tar.gz https://filebin.net/fj4f1dxzrsty771i/model.tar.gz?t=4t096hqd
export SENTIMENT_MODEL_PATH=./models_archive/sentiment_model.tar.gz
export LANGUAGE_MODEL_PATH=./models_archive/language_model.tar.gz
```

This can be also be used to train your own models using the [resources](#resources--data).

### Training the models

#### Language Model

```sh
allennlp train experiments/wiki_dataset_language.json \
    -s <path_to_experiment> \
    --include-package twitter_analysis.model \
```

#### Sentiment Analysis

```sh
allennlp train experiments/large_dataset_twitter_glove.json \
    -s <path_to_experiment> \
    --include-package twitter_analysis.model \
```


## About the model
The model is consists of pretrained a GloVe word embedding on a 6B twitter dataset, a LSTM and a feedforward network trained on the Sentiment140 dataset. The config files used to train those models can be find in the expermients folder.

## Language prediction

```python
>>> import requests

>>> url = "http://twitter-analysis.matngo.fr/process-language"
>>> r = requests.post(url, data={"tweet": "Vamos a la playa senor Zorro"})
>>> r.json()
{'all_labels': ['english', 'german', 'french', 'spanish'], 'class_probabilities': [0.01216553058475256, 0.0015967119252309203, 0.022942589595913887, 0.9632951021194458], 'label': 'spanish', 'logits': [-1.3919235467910767, -3.422583818435669, -0.7575349807739258, 2.979829788208008]}
```

## Resources / Data

- Sentiment Model weights: https://filebin.net/fj4f1dxzrsty771i/model.tar.gz?t=4t096hqd
- Language Model weights : https://filebin.net/us2f0r1sdug0glju/model.tar.gz?t=2xoime8s
- Training sets: https://filebin.net/di5b2a8bq2omgfa6/training_dataset.tar.xz?t=qiju9q5q
- Language tranining set : https://filebin.net/9joc3cauu19iic59/wiki_data.tar.gz?t=r0g103gq