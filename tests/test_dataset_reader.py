from importlib.resources import path

from twitter_analysis.model.dataset_reader import Sentiment140Reader

from . import fixtures


def test_read():
    file_path_manager = path(fixtures, "sample_dataset.txt")
    dataset_reader = Sentiment140Reader()

    instances_expected = [
        {"tweet": "J'aime les pommes", "polarity": "positive"},
        {"tweet": "Je hais les pommes", "polarity": "negative"},
        {"tweet": "Je suis neutre à propos des pommes", "polarity": "neutral"},
    ]

    with file_path_manager as file_path:
        instances = dataset_reader.read(file_path)

    assert len(instances) == 3
    for instance, expected in zip(instances, instances_expected):
        tweet = [t.text for t in instance.fields["tweet"].tokens]
        polarity = instance.fields["label"].label
        assert tweet == expected["tweet"].split(" ")
        assert polarity == expected["polarity"]
