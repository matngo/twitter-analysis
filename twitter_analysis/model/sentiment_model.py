from typing import Dict, Optional

import numpy
import torch
import torch.nn.functional as F
from allennlp.common.checks import ConfigurationError
from allennlp.data import Vocabulary
from allennlp.models.model import Model
from allennlp.modules import FeedForward, Seq2VecEncoder, TextFieldEmbedder
from allennlp.nn import InitializerApplicator, RegularizerApplicator, util
from allennlp.training.metrics import CategoricalAccuracy
from overrides import overrides


@Model.register("sentiment-classifier")
class SentimentClassifier(Model):
    """
    A binary classifier for tweet sentiment analysis.

    Parameters
        vocab: A Vocabulary, required in order to compute sizes for input/output projections.
        text_field_embedder: Used to embed the ``tokens`` ``TextField`` we get as input to the model.
        tweet_encoder: The encoder that we will use to convert the title to a vector.
        classifier_feedforward: A feed forward layer
        initializer: Used to initialize the model parameters.
        regularizer: If provided, will be used to calculate the regularization penalty during training.
    """

    def __init__(
        self,
        vocab: Vocabulary,
        text_field_embedder: TextFieldEmbedder,
        tweet_encoder: Seq2VecEncoder,
        classifier_feedforward: FeedForward,
        initializer: InitializerApplicator = InitializerApplicator(),
        regularizer: Optional[RegularizerApplicator] = None,
    ) -> None:

        super().__init__(vocab, regularizer)

        self.text_field_embedder = text_field_embedder
        self.num_classes = self.vocab.get_vocab_size("labels")
        self.tweet_encoder = tweet_encoder
        self.classifier_feedforward = classifier_feedforward

        if text_field_embedder.get_output_dim() != tweet_encoder.get_input_dim():
            raise ConfigurationError(
                "The output dimension of the text_field_embedder must match the "
                "input dimension of the tweet_encoder. Found {} and {}, "
                "respectively.".format(
                    text_field_embedder.get_output_dim(), tweet_encoder.get_input_dim()
                )
            )

        self.metrics = {"accuracy": CategoricalAccuracy()}
        self.loss = torch.nn.CrossEntropyLoss()

        initializer(self)

    @overrides
    def forward(
        self,  # type: ignore
        tweet: Dict[str, torch.LongTensor],
        label: torch.LongTensor = None,
        id: torch.LongTensor = None,
    ) -> Dict[str, torch.Tensor]:
        # pylint: disable=arguments-differ
        """
        Parameters
            tweet: The output of ``TextField.as_array()``.
            label: A variable representing the label for each instance in the batch.

        Returns
            An output dictionary consisting of:
            - class_probabilities : A tensor of shape ``(batch_size, num_classes)``
            representing a distribution over the label classes for each instance.
            - loss : A scalar loss to be optimised.
        """
        embedded_tweet = self.text_field_embedder(tweet)
        tweet_mask = util.get_text_field_mask(tweet)
        encoded_tweet = self.tweet_encoder(embedded_tweet, tweet_mask)

        logits = self.classifier_feedforward(encoded_tweet)

        output_dict = {"logits": logits}
        if label is not None:
            loss = self.loss(logits, label)
            for metric in self.metrics.values():
                metric(logits, label)
            output_dict["loss"] = loss

        return output_dict

    @overrides
    def decode(self, output_dict: Dict[str, torch.Tensor]) -> Dict[str, torch.Tensor]:
        """
        Does a simple argmax over the class probabilities, converts indices to string labels, and
        adds a ``"label"`` key to the dictionary with the result.
        """
        class_probabilities = F.softmax(output_dict["logits"], dim=-1)
        output_dict["class_probabilities"] = class_probabilities

        predictions = class_probabilities.cpu().data.numpy()
        argmax_indices = numpy.argmax(predictions, axis=-1)
        labels = [
            self.vocab.get_token_from_index(x, namespace="labels")
            for x in argmax_indices
        ]
        output_dict["label"] = labels
        return output_dict

    @overrides
    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {
            metric_name: metric.get_metric(reset)
            for metric_name, metric in self.metrics.items()
        }
