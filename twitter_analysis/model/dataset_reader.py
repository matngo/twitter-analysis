from typing import Dict, Iterable

from allennlp.data import DatasetReader
from allennlp.data.fields import LabelField, TextField
from allennlp.data.instance import Instance
from allennlp.data.token_indexers import SingleIdTokenIndexer, TokenIndexer
from allennlp.data.tokenizers import Token, Tokenizer, WordTokenizer
from overrides import overrides


@DatasetReader.register("sentiment-reader")
class Sentiment140Reader(DatasetReader):
    r"""
    Reader for the Sentiment 140 dataset.
    """

    def __init__(
        self,
        tokenizer: Tokenizer = None,
        token_indexers: Dict[str, TokenIndexer] = None,
        lazy: bool = False,
    ) -> None:
        super().__init__(lazy)
        self._tokenizer = tokenizer or WordTokenizer()
        self._token_indexers = token_indexers or {"tokens": SingleIdTokenIndexer()}
        self.polarity_map = {"0": "negative", "2": "neutral", "4": "positive"}

    @overrides
    def _read(self, file_path):
        with open(file_path, "r") as data_file:
            for i, line in enumerate(data_file):
                data = line.strip("\n").split("###")
                polarity = self.polarity_map.get(data[0])
                tweet = data[1]
                yield self.text_to_instance(tweet, polarity, i)

    @overrides
    def text_to_instance(
        self, tweet: str, polarity: str = None, id: int = None
    ) -> Instance:
        tweet_token = self._tokenizer.tokenize(tweet)
        tweet_field = TextField(tweet_token, self._token_indexers)
        fields = {"tweet": tweet_field}
        if polarity is not None:
            fields["label"] = LabelField(polarity)

        if id is not None:
            fields["id"] = LabelField(id, skip_indexing=True)

        return Instance(fields)


@DatasetReader.register("language-reader")
class WikiLanguageReader(DatasetReader):
    r"""
    Reader for the Sentiment 140 dataset.
    """

    def __init__(
        self,
        tokenizer: Tokenizer = None,
        token_indexers: Dict[str, TokenIndexer] = None,
        lazy: bool = False,
    ) -> None:
        super().__init__(lazy)
        self._tokenizer = WordTokenizer()
        self._token_indexers = token_indexers or {"tokens": SingleIdTokenIndexer()}
        self.language_map = {
            "de": "german",
            "en": "english",
            "es": "spanish",
            "fr": "french",
        }

    @overrides
    def _read(self, file_path):
        with open(file_path, "r") as data_file:
            for line in data_file:
                data = line.strip("\n").split("###")
                language = self.language_map.get(data[1])
                tweet = data[0]
                yield self.text_to_instance(tweet, language)

    @overrides
    def text_to_instance(
        self, tweet: str, language: str = None, id: int = None
    ) -> Instance:
        sentence_field = TextField(
            self._tokenizer.tokenize(tweet), self._token_indexers
        )
        fields = {"tweet": sentence_field}
        if language is not None:
            fields["label"] = LabelField(language)

        return Instance(fields)
