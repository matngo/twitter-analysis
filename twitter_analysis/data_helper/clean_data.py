import random


def clean_sentiment_140_dataset(input_path: str, output_path: str) -> None:
    r"""
    Cleans the raw data from http://cs.stanford.edu/people/alecmgo/trainingandtestdata.zip
    in order to provide the input dataset to the model.

    Input format:
        `"<polarity>","<id>","<date>","<query>","<user>","<text>"`

    Output format:
        `<text>###<polarity>`

    Arguments:
        input_path: path to the raw .csv
        output_path: path to the output dataset
    """

    with open(input_path, "rb") as fp_in, open(output_path, "w") as fp_out:
        for line in fp_in:
            try:
                data = line.decode().strip("\n").split(",")
                polarity = data[0].strip('"')
                text = data[-1].strip('"')
                if polarity != "2" and text.strip():
                    fp_out.write(f"{polarity}###{text}\n")

            except UnicodeDecodeError as e:
                pass


def subsample_data(input_path: str, output_path: str, ratio=0.005) -> None:
    with open(input_path, "r") as fp_in, open(output_path, "w") as fp_out:
        for line in fp_in:
            if random.random() < ratio:
                fp_out.write(line)
