import os
from collections import defaultdict
from typing import Dict, Iterable

from allennlp.predictors import Predictor
from twitter_analysis.model.dataset_reader import DatasetReader
from twitter_analysis.model.predictor import SentimentClassifierPredictor
from twitter_analysis.model.sentiment_model import SentimentClassifier

sentiment_predictor = SentimentClassifierPredictor.from_path(
    os.environ.get("SENTIMENT_MODEL_PATH"), "sentiment-classifier"
)

language_predictor = SentimentClassifierPredictor.from_path(
    os.environ.get("LANGUAGE_MODEL_PATH"), "sentiment-classifier"
)


def post_per_user(tweets: Iterable) -> Iterable[Dict]:
    r"""Given an iterable of tweets from the API, returns a List of dictionaries as
    ``[{label: <screen_name>, count: <tweet_counts>}]``

    Parameters:
        tweets: A sequence of tweets

    Returns:
        A sorted over the number of tweets per user.
    """
    post_count: Dict[str, int] = defaultdict(int)
    for tweet in tweets:
        post_count[tweet["user"]["screen_name"]] += 1

    sorted_post_count = sorted(
        ({"label": f"@{name}", "count": count} for name, count in post_count.items()),
        key=lambda x: x["count"],
        reverse=True,
    )

    return sorted_post_count


def post_per_lang(tweets: Iterable) -> Iterable[Dict]:
    r"""Given an iterable of tweets from the API, returns a List of dictionaries as

    ``[{label: <lang>, count: <tweet_counts>}]``

    Parameters:
        tweets: A sequence of tweets

    Returns:
        A sorted over the number of tweets per language.
    """
    post_count: Dict[str, int] = defaultdict(int)
    for tweet in tweets:
        post_count[tweet["lang"]] += 1

    sorted_post_count = sorted(
        ({"label": lang, "count": count} for lang, count in post_count.items()),
        key=lambda x: x["count"],
        reverse=True,
    )

    return sorted_post_count


def sentiment_prediction(tweets: Iterable) -> Dict[str, int]:
    r"""Given an iterable of tweets from the API, returns a Dictionary as:

    ``{positive: <positive_count>, negative: <negative_count>}``

    Parameters:
        tweets: A sequence of tweets

    Returns:
        A sorted over the number of tweets per language.
    """
    json_tweets = [
        {"tweet": tweet["text"]} for tweet in tweets if tweet["lang"] == "en"
    ]
    if json_tweets:
        sentiments = sentiment_predictor.predict_batch_json(json_tweets)
        sentiment_count: Dict[str, int] = defaultdict(int)
        for sentiment in sentiments:
            sentiment_count[sentiment["label"]] += 1

    else:
        sentiment_count = {}

    return sentiment_count
