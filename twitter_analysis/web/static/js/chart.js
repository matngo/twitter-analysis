function drawBarChart(data, elem) {
    var max = 200;
    var chart = d3.select(elem)
    var nameOffset = 150
    var countOffset = 5
    var width = chart.node().getBoundingClientRect().width
    var height = chart.node().getBoundingClientRect().height
    var topThree = data.slice(0, 5)
    var svg = chart.selectAll("svg")
        .data(topThree)
        .enter()
        .append("svg")
        .attr("class", "bar")

    svg.append("rect")
        .style("fill", "lightsteelblue")
        .attr("width", d => d.count / max * (width - nameOffset))
        .attr("height", 20)
        .attr("transform", `translate(${nameOffset}, 0)`)

    count = svg.append("g")
        .style("text-anchor", "right")
        .attr("transform", d => `translate(${nameOffset + (d.count / max * (width - nameOffset)) + countOffset}, 14)`)
        .attr("class", "count")

    count.append("text")
        .text(d => d.count)

    title = svg.append("g")
        .style("text-anchor", "left")
        .attr("transform", `translate(0, 14)`)

    title.append("text")
        .text(d => `${d.label}`)
}

function drawSentiment(data) {
    function computeWidth(sentimentCount, width) {
        var minWidth = 1
        return Math.max(minWidth, (sentimentCount / 200 / 2) * width)
    }
    var chart = d3.select("#sentiment")
    var width = chart.node().getBoundingClientRect().width - 20
    var nameOffset = 110

    title = chart.append("svg")
        .attr("class", "bar")
        .append("g")
        .attr("text-anchor", "middle")

    title.append("text")
        .attr("transform", `translate(${width / 2 - nameOffset}, 15)`)
        .text("Negative tweets")
        .attr("class", "negativeLabel")

    title.append("text")
        .attr("transform", `translate(${width / 2 + nameOffset}, 15)`)
        .text("Positive tweets")
        .attr("class", "positiveLabel")

    var svg = chart.selectAll("svg")
        .data(data)
        .enter().append("svg").attr("class", "bar")

    g = svg.append("g")
    g.append("rect")
        .style("fill", "lightsteelblue")
        .attr("width", d => computeWidth(d.positive, width))
        .attr("transform", d => `translate(${width / 2 + nameOffset / 2},0)`)
        .attr("height", 20)

    g.append("rect")
        .style("fill", "darkred")
        .attr("width", d => computeWidth(d.negative, width))
        .attr("transform", d => `translate(${width / 2 - computeWidth(d.negative, width) - nameOffset / 2},0)`)
        .attr("height", 20)

    title = svg.append("g")
        .style("text-anchor", "middle")
        .attr("transform", `translate(${width / 2}, 12)`)
        .attr("class", "title")

    title.append("text")
        .text(d => `${d.label}`)

    countPositive = svg.append("g")
        .style("text-anchor", "start")
        .attr("transform", d => `translate(${computeWidth(d.positive, width) + width / 2 + nameOffset / 2 + 5}, 15)`)
        .attr("class", "count")

    countPositive.append("text")
        .text(d => d.positive)

    countNegative = svg.append("g")
        .style("text-anchor", "end")
        .attr("transform", d => `translate(${width / 2 - computeWidth(d.negative, width) - nameOffset / 2 - 5}, 15)`)
        .attr("class", "count")

    countNegative.append("text")
        .text(d => d.negative)
}
