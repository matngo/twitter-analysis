import json
import os

from flask import Flask, g, jsonify, render_template, request
from flask_dance.contrib.twitter import make_twitter_blueprint, twitter
from twitter_analysis.data_helper.data_parser import (
    language_predictor,
    post_per_lang,
    post_per_user,
    sentiment_prediction,
    sentiment_predictor,
)

CONSUMER_KEY = os.environ.get("CONSUMER_KEY", None)
CONSUMER_KEY_SECRET = os.environ.get("CONSUMER_SECRET_KEY", None)

blueprint = make_twitter_blueprint(CONSUMER_KEY, CONSUMER_KEY_SECRET)


def get_screen_name():
    if "screen_name" not in g:
        resp = twitter.get("account/settings.json")
        g.screen_name = resp.json().get("screen_name")

    return g.screen_name


def create_app():
    app = Flask(__name__)

    app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY", "secret")
    app.config["TEMPLATES_AUTO_RELOAD"] = True
    app.register_blueprint(blueprint, url_prefix="/login")

    @app.route("/")
    def index():
        if not twitter.authorized:
            return render_template("auth.html")

        screen_name = get_screen_name()
        r = twitter.get("statuses/home_timeline.json", params={"count": 200})

        home_timeline = r.json()
        most_active_users = post_per_user(home_timeline)
        most_active_language = post_per_lang(home_timeline)
        most_sentiment = None

        top_user_tweets = [
            twitter.get(
                "statuses/user_timeline.json",
                params={"screen_name": user["label"], "count": 200},
            ).json()
            for user in most_active_users[:5]
        ]

        top_users = (user["label"] for user in most_active_users)
        sentiment_count = []

        for username, tweets in zip(top_users, top_user_tweets):
            sentiment = sentiment_prediction(tweets)
            sentiment_count.append(
                {
                    "label": username,
                    "positive": sentiment.get("positive", 0),
                    "negative": sentiment.get("negative", 0),
                }
            )

        return render_template(
            "dashboard.html",
            screen_name=screen_name,
            most_active_users=json.dumps(most_active_users),
            most_active_language=json.dumps(most_active_language),
            sentiment_count=json.dumps(sentiment_count),
        )

    @app.route("/sample_dashboard")
    def sample():
        screen_name = "blabla"
        with open("data/home_timeline.json") as fp:
            home_timeline = json.load(fp)

        with open("data/top_user_tweets.json") as fp:
            top_user_tweets = json.load(fp)

        most_active_users = post_per_user(home_timeline)
        most_active_language = post_per_lang(home_timeline)

        top_users = (user["label"] for user in most_active_users)
        sentiment_count = []

        for username, tweets in zip(top_users, top_user_tweets):
            sentiment = sentiment_prediction(tweets)
            sentiment_count.append(
                {
                    "label": username,
                    "positive": sentiment.get("positive", 0),
                    "negative": sentiment.get("negative", 0),
                }
            )

        return render_template(
            "dashboard.html",
            screen_name=screen_name,
            most_active_users=json.dumps(most_active_users),
            most_active_language=json.dumps(most_active_language),
            sentiment_count=json.dumps(sentiment_count),
        )

    @app.route("/process-sentiment", methods=("POST",))
    def process_sentiment():
        data = request.json or request.form
        return jsonify(sentiment_predictor.predict_json(data))

    @app.route("/process-language", methods=("POST",))
    def process_language():
        data = request.json or request.form
        return jsonify(language_predictor.predict_json(data))

    return app
